<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ItemTag extends Model
{
    use HasFactory;

    protected $fillable = [
        'item_id',
        'tag_id'
    ];


    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'item_tags', 'item_id', 'tag_id');
    }

    public function items(): BelongsToMany
    {
        return $this->belongsToMany(Item::class, 'item_tags', 'tag_id', 'item_id');
    }


}
