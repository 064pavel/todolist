<?php

namespace App\Http\Controllers\List\Item;

use App\Http\Controllers\Controller;
use App\Models\Dolist;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EditController extends Controller
{
    public function __invoke($list, $item)
    {
        try {
            $item = Item::where('id', $item)
                            ->with('image', 'tags')
                            ->first();

            if (!$item) {
                abort(404);
            }

            return view('content.lists.items.edit', compact('list','item'));
        } catch (\Exception $e){
            throw $e;
        }
    }
}
