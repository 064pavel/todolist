<?php

namespace App\Http\Controllers\List\Item;

use App\Http\Controllers\Controller;
use App\Models\Dolist;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DeleteController extends Controller
{
    public function __invoke($list, $item): \Illuminate\Contracts\Foundation\Application|\Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
    {
        try {
            $item = Item::findOrFail($item);
            $item->delete();

            $user_id = Dolist::where('id', $list)->pluck('user_id');

            if ($user_id[0] === Auth::id()){
                return redirect(route('lists.index'));
            } else {
                return redirect(route('lists.show', ['list' => $list]));
            }

        }catch (\Exception){
            dd('error');
        }
    }
}
