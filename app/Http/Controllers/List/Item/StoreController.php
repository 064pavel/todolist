<?php

namespace App\Http\Controllers\List\Item;

use App\Http\Controllers\Controller;
use App\Models\Dolist;
use App\Models\Image;
use App\Models\Item;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Throwable;

class StoreController extends Controller
{

    public function __invoke(Request $request): \Illuminate\Contracts\Foundation\Application|\Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
    {
        DB::beginTransaction();

        try {
            if ($request->tags) {
                $tags = explode(',', $request->tags);
            }

            $validatedData = $request->validate([
                'description' => 'required|string|max:255',
                'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
                'list_id' => 'required|numeric'
            ]);

            $item = new Item();
            $item->description = $validatedData['description'];
            $item->dolist_id = $validatedData['list_id'];
            $item->save();

            if ($request->hasFile('image')) {
                $image = $request->file('image');

                $filename = uniqid() . '.' . $image->getClientOriginalExtension();

                Storage::disk('public')->put($filename, file_get_contents($image));

                $imageModel = new Image();
                $imageModel->path = $filename;
                $imageModel->item()->associate($item);
                $imageModel->save();
            }

            if (!empty($tags)) {
                foreach ($tags as $tagName) {
                    $tag = Tag::firstOrCreate(['name' => $tagName]);

                    $item->tags()->attach($tag->id);
                }
            }

            DB::commit();

            $user_id = Dolist::where('id', $validatedData['list_id'])->pluck('user_id');
            if ($user_id[0] === Auth::id()) {
                return redirect(route('lists.index'));
            } else {
                return redirect(route('lists.show', ['list' => $validatedData['list_id']]));
            }
        } catch (Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }
}
