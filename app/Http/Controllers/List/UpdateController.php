<?php

namespace App\Http\Controllers\List;

use App\Http\Controllers\Controller;
use App\Models\Dolist;
use Illuminate\Http\Request;

class UpdateController extends Controller
{
    public function __invoke(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required|numeric',
                'title' => 'required|string|max:255',
            ]);

            $id = $request->id;
            $new_title = $request->title;

            $dolist = Dolist::findOrFail($id);
            $dolist->title = $new_title;
            $dolist->save();

            return redirect()->route('lists.index');
        }catch (\Exception $e){
            throw $e;
        }
    }
}
