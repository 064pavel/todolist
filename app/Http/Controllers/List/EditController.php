<?php

namespace App\Http\Controllers\List;

use App\Http\Controllers\Controller;
use App\Models\Dolist;

class EditController extends Controller
{
    public function __invoke($list): \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
    {
        try {
            $list = Dolist::find($list);
            return view('content.lists.edit', compact('list'));
        } catch (\Exception $e){
            throw $e;
        }
    }
}
