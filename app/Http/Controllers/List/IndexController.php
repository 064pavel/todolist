<?php

namespace App\Http\Controllers\List;

use App\Http\Controllers\Controller;
use App\Http\Resources\ListResource;
use App\Models\Dolist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function __invoke(): \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
    {
        $lists = Dolist::where('user_id', Auth::id())->with('items.image', 'items.tags')->get();

        return view('content.lists.index', compact('lists'));
    }
}
