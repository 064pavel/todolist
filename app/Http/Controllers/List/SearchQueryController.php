<?php

namespace App\Http\Controllers\List;

use App\Http\Controllers\Controller;
use App\Models\Item;
use Illuminate\Http\Request;


class SearchQueryController extends Controller
{
    public function __invoke(Request $request, $list): \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
    {
        try {
            $querySearch = $request->query('querySearch');
            $tags = $request->query('tags');

            $filteredItems = Item::where('dolist_id', $list);

            $filteredItems = $filteredItems->when($querySearch, function ($query) use ($querySearch) {
                return $query->where('description', 'like', '%' . $querySearch . '%');
            });

            if ($tags) {
                $tagsArray = explode(',', $tags);

                $filteredItems = $filteredItems->orWhereHas('tags', function ($query) use ($tagsArray) {
                    $query->whereIn('tags.name', $tagsArray);
                });
            }

            $filteredItems = $filteredItems->with('image', 'tags')->get();

            $data = $filteredItems->map(function ($item) {
                $image = $item->image ? $item->image->pluck('path')->toArray() : null;

                return [
                    'id' => $item->id,
                    'description' => $item->description,
                    'image' => $image,
                    'tags' => $item->tags->pluck('name')->toArray(),
                ];
            });

            return view('content.lists.search-result', compact('data'));
        } catch (\Exception $e) {
            return redirect(route('lists.index'));
        }
    }

}
