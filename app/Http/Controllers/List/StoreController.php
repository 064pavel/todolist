<?php

namespace App\Http\Controllers\List;

use App\Http\Controllers\Controller;
use App\Http\Requests\ListRequest;
use App\Models\Dolist;
use App\Models\DolistTag;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StoreController extends Controller
{
    public function __invoke(Request $request): \Illuminate\Contracts\Foundation\Application|\Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
    {

        try {
            $user_id = Auth::id();
            $title = $request->title;

            $dolist = new Dolist();
            $dolist->title = $title;
            $dolist->user_id = $user_id;
            $dolist->save();

            return redirect(route('lists.index'));
        } catch (\Exception $e) {
            return redirect(route('lists.index'));
        }

    }
}
