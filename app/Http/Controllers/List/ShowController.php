<?php

namespace App\Http\Controllers\List;

use App\Http\Controllers\Controller;
use App\Models\Dolist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShowController extends Controller
{
    public function __invoke(Request $request, $list)
    {
        $list = Dolist::where('id', $list)->with('items.image', 'items.tags')->get();
        return view('content.lists.show', compact('list'));
    }
}
