<?php

namespace App\Http\Controllers\List;

use App\Http\Controllers\Controller;
use App\Models\Dolist;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ShareDolistController extends Controller
{
    public function __invoke(Dolist $list): \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application | RedirectResponse
    {
        try {
            $users = User::where('id', '!=', auth()->user()->id)->get();

            return view('content.lists.share', compact('list', 'users'));
        } catch (\Exception $e) {
            return back()->with('error', 'An error occurred while sharing the list.');
        }
    }
}
