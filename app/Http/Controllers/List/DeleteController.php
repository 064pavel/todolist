<?php

namespace App\Http\Controllers\List;

use App\Http\Controllers\Controller;
use App\Models\Dolist;

class DeleteController extends Controller
{
    public function __invoke($list)
    {
        try {
            $list = Dolist::findOrFail($list);
            $list->delete();
            return redirect(route('lists.index'));
        }catch (\Exception){
            dd('error');
        }
    }
}
