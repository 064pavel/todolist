<?php

namespace App\Http\Controllers\List;

use App\Http\Controllers\Controller;
use App\Models\Dolist;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;

class GrantAccessController extends Controller
{
    public function __invoke(Request $request, Dolist $list): \Illuminate\Http\RedirectResponse
    {
        try {
            $user = User::findOrFail($request->input('user_id'));
            $canEdit = $request->has('can_edit');

            DB::table('dolist_users')->updateOrInsert(
                [
                    'dolist_id' => $list->id,
                    'user_id' => $user->id,
                ],
                [
                    'can_edit' => $canEdit,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]
            );

            return redirect()->route('lists.index');
        } catch (Throwable $e) {
            return redirect()->back()->with('error', 'An error occurred while granting access.');
        }
    }
}
