<?php

namespace App\Http\Middleware;

use App\Models\Dolist;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckDolistAccessShowMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle($request, Closure $next)
    {
        $dolistId = $request->route('list');
        $dolist = Dolist::find($dolistId);

        if (!$dolist) {
            abort(404, 'Dolist not found');
        }

        if ($dolist->user_id === auth()->id()) {
            return $next($request);
        }

        $sharedList = auth()->user()->shared_dolists()->where('dolist_id', $dolistId)->first();
        if ($sharedList) {
            return $next($request);
        }

        abort(403, 'Access denied');
    }
}
