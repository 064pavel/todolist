<?php

namespace App\Http\Middleware;

use App\Models\Dolist;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class CheckListOwnershipMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle($request, Closure $next): Response
    {
        $listId = $request->route('list');
        $list = Dolist::find($listId);

        if (!$list || $list->user_id !== Auth::id()) {
            abort(403, 'Access denied');
        }

        return $next($request);
    }
}
