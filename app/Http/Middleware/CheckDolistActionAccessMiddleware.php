<?php

namespace App\Http\Middleware;

use App\Models\Dolist;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckDolistActionAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $dolistId = $request->route('list');
        $dolist = Dolist::find($dolistId);

        if (!$dolist) {
            abort(404, 'Dolist not found');
        }

        if ($dolist->user_id === auth()->id() || $this->canEditDolist($dolistId)) {
            return $next($request);
        }

        abort(403, 'Access denied');
    }

    private function canEditDolist($dolistId)
    {
        return auth()->user()->shared_dolists()->where('dolist_id', $dolistId)->where('can_edit', true)->exists();
    }
}
