<?php

use App\Http\Controllers\List\DeleteController;
use App\Http\Controllers\List\EditController;
use App\Http\Controllers\List\IndexController;
use App\Http\Controllers\List\Item\CreateController;
use App\Http\Controllers\List\SearchQueryController;
use App\Http\Controllers\List\ShowController;
use App\Http\Controllers\List\StoreController;
use App\Http\Controllers\List\UpdateController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return redirect()->route('lists.index');
    } else {
        return view('welcome');
    }
})->name('welcome');

Auth::routes();

Route::middleware('auth')->group(function (){

    Route::prefix('lists')->group(function (){
        Route::get('/', IndexController::class)->name('lists.index');
        Route::post('/', StoreController::class)->name('lists.store');
        Route::get('/{list}', ShowController::class)->name('lists.show')->middleware('listAccessShow');
        Route::get('/{list}/edit', EditController::class)->middleware('listAccessAction')->name('lists.edit');
        Route::patch('/', UpdateController::class)->name('lists.update');
        Route::delete('/{list}', DeleteController::class)->middleware('listAccessAction')->name('lists.delete');

        Route::get('/{list}/share', \App\Http\Controllers\List\ShareDolistController::class)->name('lists.share');
        Route::post('/{list}/grant-access', \App\Http\Controllers\List\GrantAccessController::class)->name('lists.grant-access');
        Route::get('/{list}/search', SearchQueryController::class)->name('lists.search');


        Route::get('/{list}/items/create', CreateController::class)->middleware('listAccessAction')->name('lists.items.create');
        Route::post('/{list}/items', \App\Http\Controllers\List\Item\StoreController::class)->middleware('listAccessAction')->name('lists.items.store');
        Route::get('/{list}/items/{item}/edit', \App\Http\Controllers\List\Item\EditController::class)->middleware('listAccessAction')->name('lists.items.edit');
        Route::patch('/{list}/items}', \App\Http\Controllers\List\Item\UpdateController::class)->middleware('listAccessAction')->name('lists.items.update');
        Route::delete('/{list}/items/{item}', \App\Http\Controllers\List\Item\DeleteController::class)->middleware('listAccessAction')->name('lists.items.delete');
    });

});
