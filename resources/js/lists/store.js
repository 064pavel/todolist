$(document).ready(function() {
    $('#listForm').submit(function(event) {
        event.preventDefault();

        const title = $('#title').val();
        const tags = $('#tags').val();

        $.ajax({
            url: '/lists',
            method: 'POST',
            data: {
                title: title,
                tags: tags
            },
            success: function(response) {
                console.log('Список сохранен');
            },
            error: function(xhr, status, error) {
                console.error('Произошла ошибка при сохранении списка');
            }
        });
    });
});
