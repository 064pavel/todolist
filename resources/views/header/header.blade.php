<nav class="navbar navbar-expand navbar-custom header">
    <div class="container">

        <a class="navbar-brand header" href="{{ route('welcome') }}">todolist</a>

        <ul class="navbar-nav ml-auto">
            @auth
                <li class="nav-item">
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <button class="btn btn-danger" type="submit">
                            <span class="signout">Sign Out</span>
                        </button>
                    </form>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">Sign In</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">Sign Up</a>
                </li>
            @endauth
        </ul>
    </div>
</nav>
<style>
    .header{
        font-size: 26px;
    }

    .signout{
        font-size: 20px;
    }
</style>

