@extends('welcome')

@section('content')
    <div class="container mt-4 pb-4">

        @if ($list->isEmpty())
            <div class="">
                <div class="empty-description">You don't have any lists yet</div>
            </div>
        @else
            <div class="row mt-5 container-lg mx-auto justify-content-center text-center pb-4">
                @foreach ($list as $dolist)

                    <div class="col-md-4 mt-5 p-4 wrapper">
                        <div class="d-flex flex-column">
                            <div class="h1">{{ $dolist->title }}</div>

                            <div class="text-danger h4">
                                <form action="{{ route('lists.delete', ['list' => $dolist->id]) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="text-danger fw-bold" type="submit">
                                        Drop
                                    </button>
                                </form>
                            </div>


                            <div class="text-success h4 fw-bold">
                                <a href="{{ route('lists.edit', ['list' => $dolist->id]) }}"
                                   class="text-success fw-bold">
                                    Edit
                                </a>
                            </div>

                            <form action="{{ route('lists.search', ['list' => $dolist->id]) }}" method="GET" class="mb-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="query" class="form-label">Search:</label>
                                        <input type="text" name="querySearch" id="query" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="tags" class="form-label">Filter by Tags:</label>
                                        <input type="text" name="tags" id="tags" class="form-control">
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </form>

                            <div class="d-flex flex-column">
                                @foreach ($dolist->items as $item)

                                    <div class="p-4 border border-black">
                                        <div>
                                            @if ($item->image)
                                                <div>
                                                    <a href="/storage/{{ $item->image->path }}" target="_blank">
                                                        <img class="image" src="/storage/{{ $item->image->path }}">
                                                    </a>
                                                </div>
                                            @else
                                                <div>No Image</div>
                                            @endif
                                        </div>
                                        <div class="h3">
                                            <div>ID: {{ $item->id }}</div>
                                            <div>
                                                {{ $item->description }}
                                            </div>
                                        </div>
                                        <div class="d-flex">
                                            @foreach ($item->tags as $tag)
                                                <div class="mx-2 h5">#{{ $tag->name }}</div>
                                            @endforeach
                                        </div>

                                        <div class="d-flex">
                                            <div class="text-danger h4 m-4">
                                                <form
                                                    action="{{ route('lists.items.delete', ['list' => $dolist->id, 'item' => $item->id]) }}"
                                                    method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="text-danger fw-bold" type="submit">
                                                        Drop
                                                    </button>
                                                </form>
                                            </div>

                                            <div class="text-success h4 fw-bold m-4">
                                                <a href="{{ route('lists.items.edit', ['list' => $dolist->id, 'item' => $item->id]) }}"
                                                   class="text-success fw-bold">
                                                    Edit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="w-100 mt-auto align-self-end mb-5">
                                <a href="{{ route('lists.items.create', ['list' => $dolist->id]) }}"
                                   class="btn btn-success w-100">
                                    +
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
@endsection

<style>
    .empty-description {
        font-size: 22px;
    }

    .wrapper {
        height: 300px;
        width: 140px;
    }

    .image {
        height: 150px;
        width: 150px;
        margin: 10px;
        border: 1px solid #000;
    }
</style>
