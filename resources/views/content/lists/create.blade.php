<div class="container mt-4">
    <h1>Create a List</h1>

    <form action="{{route('lists.store')}}" id="listForm" method="post">
        @csrf
        <div class="form-group mb-2">
            <label for="title">Title:</label>
            <input type="text" class="form-control" name="title" id="title" placeholder="Enter title">
        </div>
        <button type="submit" class="btn btn-primary">Create List</button>
    </form>
</div>
