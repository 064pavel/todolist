@extends('welcome')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <form method="POST" action="{{ route('lists.grant-access', ['list' => $list->id]) }}">
                    @csrf

                    <div class="mb-3 h4">
                        After opening access, send the user a link: <br> <span class="text-primary">http://194.58.119.117/lists/{{$list->id}}</span>
                    </div>
                    <div class="form-group">
                        <label for="user_id" class="my-2 h3">User:</label>
                        <select name="user_id" id="user_id" class="form-control my-2" required>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group form-check my-2">
                        <input type="checkbox" class="form-check-input" name="can_edit" id="can_edit">
                        <label class="form-check-label" for="can_edit">Can Edit</label>
                    </div>

                    <button type="submit" class="btn btn-primary">Share</button>
                </form>
            </div>
        </div>
    </div>
@endsection
