@extends('welcome')

@section('content')
    <div class="container">

        <form action="{{ route('lists.items.store', ['list' => $list]) }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="mb-3">
                <label for="content" class="form-label">Content</label>
                <input type="text" class="form-control" id="content" name="description" required>
            </div>

            <div class="mb-3">
                <label for="tags" class="form-label">Tags (comma-separated)</label>
                <input type="text" class="form-control" id="tags" name="tags" required>
            </div>

            <div class="mb-3">
                <label for="image" class="form-label">Image</label>
                <input type="file" class="form-control" id="image" name="image">
            </div>

            <input type="hidden" value="{{$list}}" name="list_id">

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>


    </div>
@endsection
