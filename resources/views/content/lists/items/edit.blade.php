@extends('welcome')

@section('content')
    <div class="container">

        <form action="{{ route('lists.items.update', ['list' => $list]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')

            <div class="mb-3">
                <label for="content" class="form-label">Content</label>
                <input type="text" class="form-control" id="content" value="{{$item->description}}" name="description" required>
            </div>

            <div class="mb-3">
                <label for="tags" class="form-label">Tags (comma-separated)</label>
                <input type="text" class="form-control" id="tags" value="{{ $item->tags->pluck('name')->implode(',') }}" name="tags" required>
            </div>

            <div class="mb-3">
                <label for="image" class="form-label">Image</label>
                <input type="file" class="form-control" id="image" name="image">
            </div>

            <input type="hidden" value="{{$list}}" name="list_id">
            <input type="hidden" value="{{$item->id}}" name="item_id">

            <button type="submit" class="btn btn-primary">Update</button>
        </form>


    </div>
@endsection
