@extends('welcome')

@section('content')
    <div class="container mt-4">
        <h1>Edit a List</h1>

        <form action="{{route('lists.update')}}" id="listForm" method="post">
            @csrf
            @method('PATCH')
            <div class="form-group mb-2">
                <label for="title">Title:</label>
                <input type="text" class="form-control" value="{{$list->title}}" name="title" id="title" placeholder="Enter title">
                <input type="hidden" name="id" value="{{$list->id}}">
            </div>
            <button type="submit" class="btn btn-success">Update List</button>
        </form>
    </div>

@endsection
