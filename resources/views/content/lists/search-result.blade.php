@extends('welcome')

@section('content')
    <div class="container mt-5">
        @if(count($data) === 0)
            <div class="h1 fw-bold">Nothing found for your request</div>
        @else
            <div>
                <div>
                    <a class="h3 text-primary" href="{{route('lists.index')}}">Back</a>
                </div>
                @foreach($data as $item)
                    <div class="mt-5 border border-black w-25 p-3">
                        <div class="h2">{{$item['description']}}</div>
                        <div class="h3">Tags: #{{implode(', ', $item['tags'])}}</div>
                        @if($item['image'])
                            <div>
                                <a href="/storage/{{ $item['image'][0] }}" target="_blank">
                                    <img class="image" src="/storage/{{ $item['image'][0] }}">
                                </a>
                            </div>
                        @endif


                    </div>
                @endforeach

            </div>
        @endif
    </div>

@endsection

<style>
    .image{
        height: 150px;
        width: 150px;
    }
</style>
